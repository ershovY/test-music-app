//
//  Networking.swift
//  TestMusicApp
//
//  Created by Юрий Ершов on 02.03.2020.
//  Copyright © 2020 smartProject. All rights reserved.
//

import Foundation
import Alamofire
import CommonCrypto


final class Networking {
    
    static let shared = Networking()
    
    let developerKeyAppleMusic = "RSS49LQUMZ".sha256()
    
    enum pathes: String {
        case search = "https://itunes.apple.com/search"
        case lookTracks = "https://itunes.apple.com/lookup"
    }
    
    // 1
    func fetchSongInfoFull (Search: String, completion: @escaping (songSetResponse) -> Void) {
        Alamofire.request(URL(string: pathes.search.rawValue)!,
                          method: .get, parameters: ["term":Search, "media": "music", "limit": "30"],
                          headers: ["curl -v -H 'Authorization: Bearer":developerKeyAppleMusic])
            .validate()
            .responseJSON { (response) in
                guard response.result.isSuccess, let value = response.data
                    else { print("📛❌ - response.result.isSuccess - false ");
                        return}
                //print(String(data: value, encoding: .utf8)!)
                do {
                    let info: songSetResponse = try JSONDecoder().decode(songSetResponse.self, from: value)
                    completion(info)
                } catch {
                    print("❌ - \(error.localizedDescription)")
                    debugPrint(error)
                }
        }
    }
    
    func downloadPreviewSong(string url: String, complition: @escaping (URL)-> Void) {
        
        DispatchQueue.main.async {
            let destination: DownloadRequest.DownloadFileDestination = { _, _ in
                let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                // let fileName = url
                let fileURL = documentsURL.appendingPathComponent("song.m4a")
                return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
            }
            
            Alamofire.download(url, to: destination).response { (response) in
                
                if response.error == nil {
                    guard let urlSong = response.destinationURL else { return }
                    complition(urlSong)
                }
            }
        }
    }
    
    func downloadImages(imageURL: String, comlition: @escaping (UIImage)-> Void) {
        
        Alamofire.request(imageURL, method: .get)
            .validate()
            .responseData(completionHandler: { (responseData) in
                do
                {
                    if let image = UIImage(data: responseData.data!) {
                        comlition(image) }
                }
                catch
                {
                    print("📀 Error get image\(error.localizedDescription)")
                }
            })
    }
}


// Encode in sha256 - не стал удалять - необходимо для получения скриптованной подписи в API Appple Music
extension String {
    
    func sha256() -> String{
        if let stringData = self.data(using: String.Encoding.utf8) {
            return hexStringFromData(input: digest(input: stringData as NSData))
        }
        return ""
    }
    
    private func digest(input : NSData) -> NSData {
        let digestLength = Int(CC_SHA256_DIGEST_LENGTH)
        var hash = [UInt8](repeating: 0, count: digestLength)
        CC_SHA256(input.bytes, UInt32(input.length), &hash)
        return NSData(bytes: hash, length: digestLength)
    }
    
    private  func hexStringFromData(input: NSData) -> String {
        var bytes = [UInt8](repeating: 0, count: input.length)
        input.getBytes(&bytes, length: input.length)
        
        var hexString = ""
        for byte in bytes {
            hexString += String(format:"%02x", UInt8(byte))
        }
        
        return hexString
    }
}
