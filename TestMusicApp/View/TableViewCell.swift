//
//  CollectionViewCell.swift
//  TestMusicApp
//
//  Created by Юрий Ершов on 03.03.2020.
//  Copyright © 2020 smartProject. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var Author: UILabel!
    @IBOutlet weak var TrackName: UILabel!
    
}
