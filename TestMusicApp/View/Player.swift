//
//  Pleer.swift
//  TestMusicApp
//
//  Created by Юрий Ершов on 02.03.2020.
//  Copyright © 2020 smartProject. All rights reserved.
//

import UIKit
import Alamofire
import AVFoundation

class Player: UIViewController {
    
    
    @IBOutlet weak var AlbomName: UILabel!
    @IBOutlet weak var AuthorName: UILabel!
    @IBOutlet weak var TrackName: UILabel!
    @IBOutlet weak var TimerPassed: UILabel!
    @IBOutlet weak var TimerLeft: UILabel!
    @IBOutlet weak var PlayPause: UIButton!
    @IBOutlet weak var AlbomImage: UIImageView!
    @IBOutlet weak var ProgressBar: UISlider!
    
    var infoSet: infoSet?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.ProgressBar.value = 0
        self.ProgressBar.minimumValue = 0
        self.ProgressBar.maximumValue = 1
        self.ProgressBar.minimumTrackTintColor = UIColor.lightGray

        self.AlbomName.text = infoSet?.collectionName
        self.AuthorName.text = infoSet?.artistName
        self.TrackName.text = infoSet?.trackName
        self.PlayPause.setImage(UIImage(named: "play"), for: .normal)
        self.AlbomImage.image = UIImage(named: "albom")
        AudioManager.shared.audioPlayer.delegate = self
        
        guard infoSet!.artworkUrl60 != nil else {print("artworkUrl60 is out"); return}
        
        
        Networking.shared.downloadPreviewSong(string: (infoSet?.previewUrl)!) { urlDownloadedFile in
            AudioManager.shared.prepearToPlay(url: urlDownloadedFile)
            self.PlayPause.setImage(UIImage(named: "pause"), for: .normal)
            AudioManager.shared.audioPlayer.play()
        }
        
        Networking.shared.downloadImages(imageURL: infoSet!.artworkUrl60!) { (image) in
            self.AlbomImage.image = image }
        
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateInfoPlay), userInfo: nil, repeats: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if AudioManager.shared.audioPlayer.isPlaying {
            self.PlayPause.setImage(UIImage(named: "pause"), for: .normal)
        }
    }
    
    @IBAction func PlayPauseAction(_ sender: UIButton) {
        if AudioManager.shared.audioPlayer.isPlaying {
            AudioManager.shared.audioPlayer.pause()
            self.PlayPause.setImage(UIImage(named: "play"), for: .normal)
        } else {
            AudioManager.shared.audioPlayer.play()
            self.PlayPause.setImage(UIImage(named: "pause"), for: .normal)
        }
    }
    @IBAction func ProgressAction(_ sender: UISlider) {
        AudioManager.shared.audioPlayer.currentTime = Double(sender.value) * AudioManager.shared.audioPlayer.duration
    }
}

extension Player: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        self.PlayPause.setImage(UIImage(named: "play"), for: .normal)
    }
    
    @objc func updateInfoPlay() {
        self.TimerPassed.text = Int(AudioManager.shared.audioPlayer.currentTime).description
        self.TimerLeft.text = Int((AudioManager.shared.audioPlayer.duration - AudioManager.shared.audioPlayer.currentTime)).description
        self.ProgressBar.value = Float(AudioManager.shared.audioPlayer.currentTime / AudioManager.shared.audioPlayer.duration)
        if AudioManager.shared.audioPlayer.isPlaying == false {
            self.PlayPause.setImage(UIImage(named: "play"), for: .normal)
        }
    }
}
