//
//  ViewController.swift
//  TestMusicApp
//
//  Created by Юрий Ершов on 02.03.2020.
//  Copyright © 2020 smartProject. All rights reserved.
//

import UIKit

class FindPage: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {

    var setSong: songSetResponse?
    
    @IBOutlet weak var tableViewResults: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableViewResults.showsVerticalScrollIndicator = false
        }


    //    MARK: - TableView dataSource delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.setSong == nil { return 0 } else {
            return setSong!.results.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.setSong == nil {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableViewCell
            cell.textLabel?.text = nil
            cell.Author.text = setSong!.results[indexPath.row].artistName
            cell.TrackName.text = setSong!.results[indexPath.row].trackName
            cell.layer.borderColor = UIColor.black.cgColor
            cell.layer.borderWidth = 1
            cell.layer.cornerRadius = 8
        
            return cell
        }
        
    }
    
    //    MARK: - TableView delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard setSong != nil else {return}
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc : Player = storyboard.instantiateViewController(withIdentifier: "PlayerVC") as! Player
        vc.infoSet = setSong?.results[indexPath.row]
        let navigationController = UINavigationController(rootViewController: vc)
        self.present(navigationController, animated: true, completion: nil)
    }
    
    //    MARK: - SearchBar delegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count > 3 {
            Networking.shared.fetchSongInfoFull(Search: searchText) { songSet in
                self.setSong = songSet
                self.tableViewResults.reloadData()
            }
            if self.setSong?.results.count == 0 {
                let message = UILabel(frame: self.tableViewResults.frame)
                message.text = "Результат поиска не найден"
                message.textAlignment = .center
                message.textColor = .darkGray
                self.view.addSubview(message)
                Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(removeMessage), userInfo: nil, repeats: false)
            }
        }
        
        if searchText == "" {
            self.view.endEditing(true)
            self.setSong = nil
            self.tableViewResults.reloadData()
            self.searchBar.resignFirstResponder()
        }
    }
    
    @objc func removeMessage() { self.view.subviews.last?.removeFromSuperview() }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
}

