//
//  AudioManager.swift
//  TestMusicApp
//
//  Created by Юрий Ершов on 02.03.2020.
//  Copyright © 2020 smartProject. All rights reserved.
//

import Foundation
import StoreKit
import MediaPlayer
import AVFoundation

final class AudioManager {
    
    static var shared = AudioManager()
    
    var audioPlayer = AVAudioPlayer()
    
    func prepearToPlay(url: URL) {
        do {
            self.audioPlayer = try AVAudioPlayer(contentsOf:  url)
            self.audioPlayer.prepareToPlay()
        } catch {
            print("📀 📀 Error:", error.localizedDescription)
        }
    }
    
}


